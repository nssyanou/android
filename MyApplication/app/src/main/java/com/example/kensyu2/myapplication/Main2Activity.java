package com.example.kensyu2.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.Toast;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);


        CheckBox checkBox = (CheckBox) findViewById(R.id.checkbox);         //チェックボックスを取得
        boolean checked = checkBox.isChecked();
        TextView textView = (TextView) findViewById(R.id.textview);
        if(checked ==true){
            textView.setText("選択されています");
        }else{
            textView.setText("選択されていません");
        }

        // 選択されているRadioButonのIDを取得する
// どれも選択されていなければgetCheckedRadioButtonIdは-1が返ってくる
// radioGroupは確認しようとしているRadioGroupクラス（RadioGroupOsかRadioGroupCarrier）
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.RadioGroup);
        int checkedId = radioGroup.getCheckedRadioButtonId();
        TextView textView2 = (TextView) findViewById(R.id.textview2);
        if (-1 != checkedId) {
            textView2.setText("選択されています");
        } else {
            textView2.setText("選択されていません");
        }


    }
}
