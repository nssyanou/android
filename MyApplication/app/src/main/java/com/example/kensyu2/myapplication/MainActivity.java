package com.example.kensyu2.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;



public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {            //ウインドウを開く
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    Button btn = (Button)findViewById(R.id.Button1);                // ボタンのオブジェクトを取得

    CheckBox checkBox = (CheckBox) findViewById(R.id.checkbox);

        public void onclick(View view){
            boolean checked = checkBox.isChecked();                 //チェックボックスを調べる
        if(checked == true){
            startActivity(new Intent(this,Main2Activity.class));
        }else{
            startActivity(new Intent(this,Main3Activity.class));
        }

    }


}
