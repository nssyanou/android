package com.example.kensyu2.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.ArrayAdapter;

public class Main3Activity extends AppCompatActivity {
    ListView lv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        String[] list = new String[20];                                     //配列を用意
        for(int i=0;i<20;i++){
            list[i] = "項目"+i;
        }

        lv = (ListView) findViewById(R.id.listView1);                       //中に入れる

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_expandable_list_item_1, list);

        lv.setAdapter(adapter);

    }
}