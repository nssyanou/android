package com.example.kensyu2.myapplication3;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button btn1;
    private Button btn2;
    private Button btn3;
    private Button btn4;
    private Button btn5;
    private Button btn6;
    private Button btn7;
    private Button btn8;
    private Button btn9;
    private Button btn0;
    private Button btnDot;
    private Button btnAC;
    private Button btnPM;
    private Button btnPer;
    private Button btnDiv;
    private Button btnkake;
    private Button btnmai;
    private Button btnPlus;
    private Button btnequ;

    private String num1 = "";
    private String num2 = "";
    private String ope = "";
    private String answer = null;
    boolean flg = true;
    int n;

    @Override
    protected void onCreate(Bundle  savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn1 = (Button) findViewById(R.id.button1);
        btn1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                n=1;
                TextView display = (TextView) findViewById(R.id.display);
                if(true==flg) {     //二桁入力対応
                    display.setText(display.getText() + Integer.toString(n));
                }else{     //演算子の直後の場合
                    display.setText(Integer.toString(n));
                    flg=true;
                }
            }
        }  );
        btn2 = (Button) findViewById(R.id.button2);
        btn2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                n=2;
                TextView display = (TextView) findViewById(R.id.display);
                if(true==flg) {     //二桁入力対応
                    display.setText(display.getText() + Integer.toString(n));
                }else{     //演算子の直後の場合
                    display.setText(Integer.toString(n));
                    flg=true;
                }
                if(num1!=""){
                    num2 = display.getText()+Integer.toString(n);}
            }
        }  );
        btn3 = (Button) findViewById(R.id.button3);
        btn3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                n=3;
                TextView display = (TextView) findViewById(R.id.display);
                if(true==flg) {     //二桁入力対応
                    display.setText(display.getText() + Integer.toString(n));
                }else{     //演算子の直後の場合
                    display.setText(Integer.toString(n));
                    flg=true;
                }
                if(num1!=""){
                    num2 = display.getText()+Integer.toString(n);}
            }
        }  );
        btn4 = (Button) findViewById(R.id.button4);
        btn4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                n=4;
                TextView display = (TextView) findViewById(R.id.display);
                if(true==flg) {     //二桁入力対応
                    display.setText(display.getText() + Integer.toString(n));
                }else{     //演算子の直後の場合
                    display.setText(Integer.toString(n));
                    flg=true;
                }
                if(num1!=""){
                    num2 = display.getText()+Integer.toString(n);}
            }
        }  );
        btn5 = (Button) findViewById(R.id.button5);
        btn5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                n=5;
                TextView display = (TextView) findViewById(R.id.display);
                if(true==flg) {     //二桁入力対応
                    display.setText(display.getText() + Integer.toString(n));
                }else{     //演算子の直後の場合
                    display.setText(Integer.toString(n));
                    flg=true;
                }
                if(num1!=""){
                    num2 = display.getText()+Integer.toString(n);}
            }
        }  );
        btn6 = (Button) findViewById(R.id.button6);
        btn6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                n=6;
                TextView display = (TextView) findViewById(R.id.display);
                if(true==flg) {     //二桁入力対応
                    display.setText(display.getText() + Integer.toString(n));
                }else{     //演算子の直後の場合
                    display.setText(Integer.toString(n));
                    flg=true;
                }
                if(num1!=""){
                    num2 = display.getText()+Integer.toString(n);}
            }
        }  );
        btn7 = (Button) findViewById(R.id.button7);
        btn7.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                n=7;
                TextView display = (TextView) findViewById(R.id.display);
                if(true==flg) {     //二桁入力対応
                    display.setText(display.getText() + Integer.toString(n));
                }else{     //演算子の直後の場合
                    display.setText(Integer.toString(n));
                    flg=true;
                }
                if(num1!=""){
                    num2 = display.getText()+Integer.toString(n);}
            }
        }  );
        btn8 = (Button) findViewById(R.id.button8);
        btn8.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                n=8;
                TextView display = (TextView) findViewById(R.id.display);
                if(true==flg) {     //二桁入力対応
                    display.setText(display.getText() + Integer.toString(n));
                }else{     //演算子の直後の場合
                    display.setText(Integer.toString(n));
                    flg=true;
                }
                if(num1!=""){
                    num2 = display.getText()+Integer.toString(n);}
            }
        }  );
        btn9 = (Button) findViewById(R.id.button9);
        btn9.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                n=9;
                TextView display = (TextView) findViewById(R.id.display);
                if(true==flg) {     //二桁入力対応
                    display.setText(display.getText() + Integer.toString(n));
                }else{     //演算子の直後の場合
                    display.setText(Integer.toString(n));
                    flg=true;
                }
                if(num1!=""){
                    num2 = display.getText()+Integer.toString(n);}
            }
        }  );
        btn0 = (Button) findViewById(R.id.button0);
        btn0.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                n=0;
                TextView display = (TextView) findViewById(R.id.display);
                if(true==flg) {     //二桁入力対応
                    display.setText(display.getText() + Integer.toString(n));
                }else{     //演算子の直後の場合
                    display.setText(Integer.toString(n));
                    flg=true;
                }
                if(num1!=""){
                    num2 = display.getText()+Integer.toString(n);}
            }
        }  );

        btnAC = (Button) findViewById(R.id.buttonAC);
        btnAC.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView display = (TextView) findViewById(R.id.display);
                num1 = "";
                num2 = "";
                ope = "";
                answer = null;
                flg=true;
                display.setText("");
            }
        }  );
        btnPM = (Button) findViewById(R.id.buttonPM);
        btnPM.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView display = (TextView) findViewById(R.id.display);
                display.setText("+/-");
            }
        }  );
        btnPer = (Button) findViewById(R.id.buttonPer);
        btnPer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView display = (TextView) findViewById(R.id.display);
                num1 = (String)display.getText();
                int n = Integer.parseInt(num1);
                double p = (double)n / 100;
                String q = String.valueOf(p);
                display.setText(q);
            }
        }  );
        btnDiv = (Button) findViewById(R.id.buttonDiv);
        btnDiv.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView display = (TextView) findViewById(R.id.display);
                if (true == flg) {
                    ope = "÷";
                    flg = false;
                    if ("".equals(num1)) {
                        num1 = (String) display.getText();
                    } else {
                        num2 = (String) display.getText();
                        calcdivid();
                    }
                }else{
                    display.setText(display.getText());
                }
            }
        }  );
        btnkake = (Button) findViewById(R.id.buttonkake);
        btnkake.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView display = (TextView) findViewById(R.id.display);
                if (true == flg) {
                    ope = "×";
                    flg = false;
                    if ("".equals(num1)) {
                        num1 = (String) display.getText();
                    } else {
                        num2 = (String) display.getText();
                        calcmult();
                    }
                }else{
                    display.setText(display.getText());
                }
            }
        }  );
        btnmai = (Button) findViewById(R.id.buttonmai);
        btnmai.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView display = (TextView) findViewById(R.id.display);
                if (true == flg) {
                    ope = "-";
                    flg = false;
                    if ("".equals(num1)) {
                        num1 = (String) display.getText();
                    } else {
                        num2 = (String) display.getText();
                        calcMinus();
                    }
                }else{
                    display.setText(display.getText());
                }
            }
        }  );
        btnPlus = (Button) findViewById(R.id.buttonPlus);
        btnPlus.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView display = (TextView) findViewById(R.id.display);
                if (true==flg) {
                    ope = "+";
                    flg = false;
                    if ("".equals(num1)) {
                        num1 = (String) display.getText();
                    } else {
                        num2 = (String) display.getText();
                        calcPlus();
                    }
                }else{
                    display.setText(display.getText());
                }
            }
        }  );
        btnequ = (Button) findViewById(R.id.buttonequ);
        btnequ.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView display = (TextView) findViewById(R.id.display);
                flg=false;
                if(ope=="÷"){
                    num2 = (String) display.getText();
                    calcdivid();
                }
                if(ope=="×"){
                    num2 = (String) display.getText();
                    calcmult();
                }
                if(ope=="-"){
                    num2 = (String) display.getText();
                    calcMinus();
                }
                if(ope=="+"){
                    num2 = (String) display.getText();
                    calcPlus();
                    }
                }
        }  );

    }
    void calcdivid(){
        TextView display = (TextView) findViewById(R.id.display);
        if(answer == null) {
            int n1 = Integer.parseInt(num1);
            int n2 = Integer.parseInt(num2);
            int a = n1 / n2;
            answer = Integer.toString(a);
            display.setText(answer);
        }else{
            int a = Integer.parseInt(answer) / Integer.parseInt(num2);
            answer = Integer.toString(a);
            display.setText(answer);
        }
    }
    void calcmult(){
        TextView display = (TextView) findViewById(R.id.display);
        if(answer == null) {
            int n1 = Integer.parseInt(num1);
            int n2 = Integer.parseInt(num2);
            int a = n1 * n2;
            answer = Integer.toString(a);
            display.setText(answer);
        }else{
            int a = Integer.parseInt(answer) * Integer.parseInt(num2);
            answer = Integer.toString(a);
            display.setText(answer);
        }
    }
    void calcMinus(){
        TextView display = (TextView) findViewById(R.id.display);
        if(answer == null) {
            int n1 = Integer.parseInt(num1);
            int n2 = Integer.parseInt(num2);
            int a = n1 - n2;
            answer = Integer.toString(a);
            display.setText(answer);
        }else{
            int a = Integer.parseInt(answer) - Integer.parseInt(num2);
            answer = Integer.toString(a);
            display.setText(answer);
        }
    }
    void calcPlus(){
        TextView display = (TextView) findViewById(R.id.display);
        if(answer == null) {
                int n1 = Integer.parseInt(num1);
                int n2 = Integer.parseInt(num2);
                int a = n1 + n2;
                answer = Integer.toString(a);
                display.setText(answer);
        }else{
            int a = Integer.parseInt(answer) + Integer.parseInt(num2);
            answer = Integer.toString(a);
            display.setText(answer);
        }

    }

}
